# simpleStratum



## Getting started

In docker folder are the .yaml that you send me to build my DB. And the int.sql with the queries that the project requires.
First you need to run it to get the data base prepared for the server.

# SimpleStratum project

Here we have 2 folders client and server.

I used the client main.go to simulate a client call to the server. Is a simple and hardcode call.

To run and simulate the calls and response are some steps:

    * Firstly you need to go to the terminal project and run this: 
    `go run server/main.go`
    * After that command you have the server running and be ready to call it, so you cand do it by this:
    `go run client/main.go`

Then you will have the response from the server.

This a simple hardcode calling server
