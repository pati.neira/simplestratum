package server_database

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"time"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "luxor"
	password = "luxor"
	dbname   = "luxor"
)

type RequestDB struct {
	Id              int    `db:"id"`
	UserName        string `db:"username"`
	TransactionDate string `db:"transaction_date"`
}

func InsertRequest(nameReq string) (*sql.DB, error) {
	var err error

	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed to connect to the DB :%s", err))
	}

	// close database
	defer db.Close()

	// check db
	err = db.Ping()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed ping :%s", err))
	}

	fmt.Println("Connected!")

	insertDynStmt := `insert into authorize.request("id", "username", "transaction_date") values($1, $2, $3)`
	_, err = db.Exec(insertDynStmt, 3, nameReq, time.Now().Format("2006-01-02 15:04:05")) //id wanted to be sequence
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed insert :%s", err))
	}

	return db, nil
}
