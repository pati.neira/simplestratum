package main

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"simpleStratum/server/database"
)

type Request struct {
	//JsonRpc string            `json:"jsonrpc"` // Version of the JSON RPC protocol, always set to 2.0
	Id     int               `json:"id"`     // Auto incrementing ID number for this request
	Method string            `json:"method"` // Remote procedure name to invoke on the server
	Params map[string]string `json:"params"` // List of parameters to pass through (keep types simple)
}

type Response struct {
	//JsonRpc string          `json:"jsonrpc"` // Version of the JSON RPC protocol, always set to 2.0
	Id     int             `json:"id"`     // Auto incrementing ID number for this request
	Error  json.RawMessage `json:"error"`  // Any error returned by the remote side
	Result bool            `json:"result"` // Whatever the remote side sends us in reply
}

type Mining string

func (t *Mining) Api(args *Request, reply *Response) error {
	switch args.Method {
	case "mining.authorize":
		reply.Id = args.Id + 1
		reply.Result = true
		server_database.InsertRequest(args.Params["username"])
		return nil
	default:
		reply.Id = 0
		reply.Result = false
	}
	return nil
}

func main() {

	// create and register the rpc
	banner := new(Mining)
	rpc.Register(banner)
	rpc.HandleHTTP()

	// set a port for the server
	port := ":1122"

	// listen for requests on 1122
	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal("listen error: ", err)
	}

	http.Serve(listener, nil)
}
