package main

import (
	"encoding/json"
	"log"
	"net/rpc"
)

type Request struct {
	Id     int               `json:"id"`     // Auto incrementing ID number for this request
	Method string            `json:"method"` // Remote procedure name to invoke on the server
	Params map[string]string `json:"params"` // List of parameters to pass through (keep types simple)
}

type Response struct {
	Id     int             `json:"id"`     // Auto incrementing ID number for this request
	Error  json.RawMessage `json:"error"`  // Any error returned by the remote side
	Result bool            `json:"result"` // Whatever the remote side sends us in reply
}

func main() {

	hostname := "localhost"
	port := ":1122"

	var reply Response

	params := map[string]string{}
	params["username"] = "pati"
	params["password"] = "1234"

	args := Request{
		Id:     1,
		Method: "mining.authorize",
		Params: params,
	}

	client, err := rpc.DialHTTP("tcp", hostname+port)
	if err != nil {
		log.Fatal("dialing: ", err)
	}

	err = client.Call("Mining.Api", args, &reply)
	if err != nil {
		log.Fatal("error", err)
	}

	// log the result
	resp, err := json.Marshal(reply)
	if err != nil {
		log.Fatal("error", err)
	}
	log.Printf("%s\n", resp)
}
