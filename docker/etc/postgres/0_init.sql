
--- change / add / modify this schema as necessary
CREATE TABLE public.sample_table (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    sample VARCHAR(255)
);

CREATE SCHEMA AUTHORIZE;
CREATE TABLE authorize.request (id  INT NOT NULL,
	username VARCHAR(40),
	transaction_date VARCHAR(40)
);

INSERT INTO AUTHORIZE.REQUEST (ID, username, transaction_date)
	VALUES 
		(1, 'userOne', '2022-10-09 09:32:37'),
		(2, 'userTwo', '2022-11-09 09:32:37');